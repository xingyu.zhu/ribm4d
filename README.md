# RIBM4D-GPU

Xingyu Zhu, Ziyi Wang, Eddy Lin, Yuval Medina

This is the product of the final project **Rotational Invariant BM4D with CUDA Acceleration** of Duke CS590/390 (Parallel Computing) 2020 Fall. This work is done in collaboration with Professor [Alberto Bartesaghi](https://www.cs.duke.edu/people/faculty/298) and Alejandro Silva.

The main structure of the code is inherited from the [BM4D-GPU](https://github.com/Logrus/BM4D-GPU) by Vladislav Tananaev. The cuSOFT package is based on [SOFT2.0](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.116.2325) package by Peter Kostelec and Daniel Rockmore.

The contribution on coding is available [here](https://gitlab.cs.duke.edu/xingyu.zhu/ribm4d/-/graphs/master).

### Hardware requirements
To run the ribm4d algorithm, you need a machine with a CUDA device with compute capability at least 6.1 (K80 does not work, but RTX2080ti works, since the implementation requires hardware support on 3D texture memory).

### Run the code

A executable binary of the RIBM4D algorithm is available in `./RIBM4D/ribm4d-gpu`. To execute the algorithm, you can launch it directly by bash from folder `./RIBM4D` by
```sh
./ribm4d-gpu ./data/3j7h_cropped_center_avi/3j7h_center_noisy_0.20.avi ./data/3j7h_cropped_center_avi/3j7h_center_original.avi
```
This command runs RIBM4D on the input noisy 50x50x50 volume data `./data/3j7h_cropped_center_avi/3j7h_center_noisy_0.20.avi`, and compares it with the ground truth `./data/3j7h_cropped_center_avi/3j7h_center_original.avi`

You will have an output like this
```
Device Number: 0
  Device name: Tesla P100-PCIE-12GB
  Memory Clock Rate (KHz): 715000
  Memory Bus Width (bits): 3072
  Peak Memory Bandwidth (GB/s): 549.120000

Parameters:
           input file: ./data/3j7h_cropped_center_avi/3j7h_center_noisy_0.20.avi
          output file: 
     groud truth file: ./data/3j7h_cropped_center_avi/3j7h_center_original.avi
 similarity threshold: 2500.000
       hard threshold: 2.700
          window size: 5
            step size: 3
 max cubes in a group: 16
           patch size: 4
Volume size: (50, 50, 50) total: 125000

Run first step of BM4D: 
Allocated 4913 elements for d_nstacks
=== GPU RAM ===
 mem free  12483690496  - 11905.375000 MB
 mem total 12790923264  - 12198.375000 MB
 mem used 293.000000 MB
===============

Initialize masks took: 2061635468479118989529207275520.000
=== GPU RAM ===
 mem free  12483690496  - 11905.375000 MB
 mem total 12790923264  - 12198.375000 MB
 mem used 293.000000 MB
===============

automatic batchsize (depth) 17, batchsize 4913, patch count 4913
Allocated Memory to CUSOFT Workspaces
Pre-Allocated Memory to CUSOFT Workspaces (lib)
=== GPU RAM ===
 mem free  11665801216  - 11125.375000 MB
 mem total 12790923264  - 12198.375000 MB
 mem used 1073.000000 MB
===============

Copying to device (3d tex)
Texture Memory Binded
average difference in synchronization: 0.000001
Texture Memory Binded

Start blockmatching (rot)
Total number of reference patches 4913
Finished: patch 101 / 4913
Begin: patch 1000 / 4913
Finished: patch 1000 / 4913
Begin: patch 2000 / 4913
Finished: patch 2000 / 4913
Begin: patch 3000 / 4913
Finished: patch 3000 / 4913
Begin: patch 4000 / 4913
Finished: patch 4000 / 4913
Rotational Invariant Blockmatching took: 1179.044
freeing memory by block matching and cusoft
=== GPU RAM ===
 mem free  12481593344  - 11903.375000 MB
 mem total 12790923264  - 12198.375000 MB
 mem used 295.000000 MB
===============

Texture Memory Binded
arraysize:4913, 127, 512
Sum of patches: 76821
gather_stacks_sum_rot:76821, 127, 512
Gathering cubes took: 0.002

Begin Aggregation Kernel Call
Begin Normalization
Aggregation took: 0.006
RIBM4D total time: 1179.592
PSNR noisy: 17.148
PSNR denoised: 19.617
PSNR reconstructed: 18.204
```

The last 4 lines shows the total runtime and PSNR results.
Running the algorithm takes about 1200s on P100 and about 400s on RTX2080ti or V100.

### Another way of running the code

Yes, we have a helper script. Just execute `python3 test_script.py` and you will see the log file in `./sbatchlogs`