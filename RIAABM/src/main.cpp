#include <iostream>
#include <string>
#include <riaabm/avireader.h>

int main(int argc, char* argv[]) {
    std::string fl_name = "../../RIBM4D/data/3j7h_cropped_center_avi/3j7h_center_noisy_0.05.avi";
    std::cout << fl_name << std::endl;

    std::vector<unsigned char> noisy_input;
    int data_width, data_height, data_depth;
    read_video(fl_name, noisy_input, data_width, data_height, data_depth);
    return EXIT_SUCCESS;
}