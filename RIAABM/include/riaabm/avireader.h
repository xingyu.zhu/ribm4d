#pragma once
#include <cstdlib>  // EXIT_SUCESS, EXIT_FAILURE
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

void read_video(const std::string& filename, std::vector<unsigned char>& volume, int& width, int& height, int& depth);